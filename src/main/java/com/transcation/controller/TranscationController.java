package com.transcation.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.transcation.entity.Transaction;
import com.transcation.service.TranscationService;

@RestController
public class TranscationController {

	@Autowired
	TranscationService transcationService;	

	@GetMapping("/transactions")
	public List<Transaction> getAll() {
		return transcationService.findAll();
	}
	
	
	@GetMapping("/transactions/{customerId}")
	public List<Transaction> getByCustId(@PathVariable Long customerId) {
		return transcationService.findByCustomerId(customerId);
	}

	@GetMapping("/transactions/{customerId}/{accountId}")
	public List<Transaction> getByCustIdAndAccId(@PathVariable Long customerId, @PathVariable Long accountId) {
		return transcationService.findByCustomerIdAndAccountId(customerId, accountId);
	}
}
