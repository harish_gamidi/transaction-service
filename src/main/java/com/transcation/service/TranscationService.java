package com.transcation.service;

import java.util.List;

import com.transcation.entity.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.transcation.repository.TransactionRepository;

@Component
public class TranscationService {

	@Autowired
	private TransactionRepository transactionRepository;
	
	@Autowired
    private JdbcTemplate jtm;
	

	public List<Transaction> findAll() {
		return transactionRepository.findAll();
	}

	public List<Transaction> findByCustomerId(Long customerId) {
		return transactionRepository.findByCustomerId(customerId);
	}

	public List<Transaction> findByCustomerIdAndAccountId(Long customerId, Long accountId) {
		return transactionRepository.findByCustomerIdAndAccountId(customerId, accountId);
	}
}
