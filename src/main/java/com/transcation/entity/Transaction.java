package com.transcation.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@Table(name = "TRANSACTIONS")
@NoArgsConstructor
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter
	@Setter
	private Long transactionId;

	@Getter
	@Setter
	private Long accountId;

	private String type = "CREDIT";

	@Getter
	@Setter
	private Long customerId;

	@Getter
	@Setter
	private String transcationDescription;

	@Getter
	@Setter
	private Double amount;
}
