package com.transcation.repository;


import com.transcation.entity.Transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query("FROM Transaction WHERE customerId = ?1")
    List<Transaction> findByCustomerId(Long CustomerId);

    @Query("FROM Transaction WHERE customerId = ?1 AND accountId = ?2")
    List<Transaction> findByCustomerIdAndAccountId(Long CustomerId, Long AccountId);

//    @Query(value = "SELECT * FROM TRANSACTIONS WHERE customerId = :CustomerId", nativeQuery = true)
//    List<Transaction> findTransactionsByCustomerId2(Long CustomerId);
}
