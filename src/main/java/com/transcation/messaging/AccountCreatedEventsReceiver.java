package com.transcation.messaging;

import com.transcation.entity.Transaction;
import com.transcation.messaging.events.AccountCreatedEvent;
import com.transcation.repository.TransactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AccountCreatedEventsReceiver {

    @Autowired
    private TransactionRepository transactionRepository;

    @RabbitListener(queues = "accounts-created-events-queue")
    public void accountCreatedEvent(AccountCreatedEvent event) {
        log.info("Received <" + event.toString() + ">");

        if (event.getInitialCredit() > 0) {
            Transaction transaction = new Transaction();
            transaction.setAccountId(event.getAccountId());
            transaction.setAmount(event.getInitialCredit());
            transaction.setCustomerId(event.getCustomerId());
            transaction.setTranscationDescription("Initial Credit");
            transactionRepository.save(transaction);

        }
        else {
            log.info("Account has been created, but with an initial credit of 0. Therefore not making a transaction entry");
        }
    }
}
