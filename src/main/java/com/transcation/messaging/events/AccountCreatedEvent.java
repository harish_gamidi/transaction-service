package com.transcation.messaging.events;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class AccountCreatedEvent implements Serializable {

    @JsonProperty("accountId")
    Long accountId;

    @JsonProperty("customerId")
    Long customerId;

    @JsonProperty("initialCredit")
    Double initialCredit;
}
